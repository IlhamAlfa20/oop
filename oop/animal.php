<?php
    class Animal {
        public $name;
        public $legs = 2;
        public $cold_blooded = "false";

        function __construct($name) {
            $this->name = $name;
        }

        function getName() {
            return $this->name;
        }

        function getLegs() {
            return $this->legs;
        }

        function getColdBlooded() {
            return $this->cold_blooded;
        }
    }
    
?>