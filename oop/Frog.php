<?php
    class Frog extends Animal {
        public $jump = "hop hop";
        public $legs = 4;

        function __construct($name) {
            $this->name = $name;
        }

        function getName() {
            return $this->name;
        }

        function getLegs() {
            return $this->legs;
        }

        function getJump() {
            return $this->jump;
        }
    }
    
?>