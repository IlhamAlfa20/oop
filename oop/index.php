<?php
    require 'animal.php';
    require 'Frog.php';
    require 'Ape.php';

    $sheep = new Animal('Shaun');
    echo $sheep->getName() . "<br>";
    echo $sheep->getLegs() . "<br>";
    echo $sheep->getColdBlooded() . "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo $sungokong->getName() . "<br>";
    echo $sungokong->getLegs() . "<br>";
    echo $sungokong->getYell() . "<br><br>";

    $kodok = new Frog("buduk");
    echo $kodok->getName() . "<br>";
    echo $kodok->getLegs() . "<br>";
    echo $kodok->getJump() . "<br><br>";
?>