<?php
    class Ape extends Animal {
        public $yell = "Auooo";

        function __construct($name) {
            $this->name = $name;
        }

        function getName() {
            return $this->name;
        }

        function getLegs() {
            return $this->legs;
        }

        function getYell() {
            return $this->yell;
        }
    }
?>